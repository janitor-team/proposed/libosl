Source: libosl
Section: libs
Priority: optional
Maintainer: Yann Dirson <dirson@debian.org>
Build-Depends: dpkg-dev (>= 1.16.1~), debhelper-compat (= 12), doxygen, cmake, libcppunit-dev, libboost-all-dev
Standards-Version: 4.5.0
Homepage: https://gps.tanaka.ecc.u-tokyo.ac.jp/gpsshogi/pukiwiki.php
Vcs-Git: https://salsa.debian.org/debian/libosl.git
Vcs-Browser: https://salsa.debian.org/debian/libosl

Package: libosl1v5
Architecture: i386 amd64
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libosl-doc
Conflicts: libosl1
Replaces: libosl1
Description: library for Shogi playing programs
 OpenShogiLib (OSL) provides following features of Shogi playing programs,
 especially for Shogi programming study. Shogi is a Japanese two-player board
 game like Chess.
 1. Board: reading/writing records, generating legal moves etc.
 2. Evaluation function: piece values and a lot of features
 3. Hash table: hashing boards etc.
 4. Checkmate search: concurrent df-pn+ etc.
 5. Search framework: full-depth search, realization probability etc.
 6. Move category: mate, illegal moves etc.
 7. Opening book
 8. Gaming: search time control etc.
 .
 OSL works only on Pentium4 or later compatible CPUs due to using the SSE2
 instruction set.

Package: libosl-dev
Section: libdevel
Architecture: i386 amd64
Depends: ${shlibs:Depends}, ${misc:Depends}, libosl1v5 (= ${binary:Version})
Description: library for Shogi playing programs
 OpenShogiLib (OSL) provides following features of Shogi playing programs,
 especially for Shogi programming study. Shogi is a Japanese two-player board
 game like Chess.
 1. Board: reading/writing records, generating legal moves etc.
 2. Evaluation function: piece values and a lot of features
 3. Hash table: hashing boards etc.
 4. Checkmate search: concurrent df-pn+ etc.
 5. Search framework: full-depth search, realization probability etc.
 6. Move category: mate, illegal moves etc.
 7. Opening book
 8. Gaming: search time control etc.
 .
 This package contains header files and static libraries for developers.

Package: libosl-doc
Section: doc
Architecture: all
Suggests: libosl1v5
Depends: libjs-jquery, ${misc:Depends}
Description: library for Shogi playing programs
 OpenShogiLib (OSL) provides following features of Shogi playing programs,
 especially for Shogi programming study. Shogi is a Japanese two-player board
 game like Chess.
 1. Board: reading/writing records, generating legal moves etc.
 2. Evaluation function: piece values and a lot of features
 3. Hash table: hashing boards etc.
 4. Checkmate search: concurrent df-pn+ etc.
 5. Search framework: full-depth search, realization probability etc.
 6. Move category: mate, illegal moves etc.
 7. Opening book
 8. Gaming: search time control etc.
 .
 This package contains documents that Doxygen generates from OSL source files.
